#!/bin/bash

# move into results directory
cd results

# Create array of filenames in results folder
filenames_array=(*) 

# Sort array of filenames in date order and assign to new array
sorted_date_array=($(sort -n < <(printf '%s\n' "${filenames_array[@]}")))

cd ..

# Resets rankings
cp base_player_file.json rankings.json
cp base_historic_rankings.json historic_rankings.json

# Loop through each results file and update rankings
for i in "${sorted_date_array[@]}"
do
   node ./updateRankings.js $i
   echo "-----Updated rankings for ${i}-----"
done
