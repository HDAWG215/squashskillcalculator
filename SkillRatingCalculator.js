module.exports = function(P1Sk, P2Sk, P1Sc, P2Sc) {
    
    let K = 32;

    if (P1Sk < P2Sk) {
        K = P1Sk / P2Sk * 100
    } else if (P2Sk < P1Sk) {
        K = P2Sk / P1Sk * 100
    }

    if (P1Sc > P2Sc) {
        K = K + (P1Sc - P2Sc) / (P2Sc + 1);
    } else if (P2Sc > P1Sc) {
        K = K + (P2Sc - P1Sc) / (P1Sc + 1);
    }

    const R1 = 10 ^ (P1Sk / 400);
    const R2 = 10 ^ (P2Sk / 400);

    const E1 = R1 / (R1 + R2);
    const E2 = R2 / (R1 + R2);

    const S1 = P1Sc > P2Sc ? 1 : 0;
    const S2 = P2Sc > P1Sc ? 1 : 0;

    const RR1 = P1Sk + K * (S1 - E1);
    const RR2 = P2Sk + K * (S2 - E2);

    return [Math.round(RR1), Math.round(RR2)];
};
