const fs = require('fs');
const skillRatingCalculator = require('./SkillRatingCalculator');
const currentRankings = require('./rankings.json');
const historicRankings = require('./historic_rankings.json');

const scoresToUpdateWith = require(`./results/${process.argv.slice(2)[0]}`);

// For testing purposes
// console.log(skillRatingCalculator(1600, 1575, 11, 13));

scoresToUpdateWith.forEach(function(game) {
    const scores = game.scoreline.split('-');

    const player1Score = parseInt(scores[0]);
    const player2Score = parseInt(scores[1]);

    const player1 = currentRankings.find(function(element) {
        return element.playerName === game.player1
    });

    const player2 = currentRankings.find(function(element) {
        return element.playerName === game.player2
    });

    const player1PreviousSkill = player1.skillRating; 
    const player2PreviousSkill = player2.skillRating;

    const skillAdjustments = skillRatingCalculator(
        player1.skillRating,
        player2.skillRating,
        player1Score,
        player2Score
    );

    //calculate form and matches won/lost
    if (player1Score > player2Score) {
        player1.matchesWon += 1;
        player1.lastFive = player1.lastFive.substring(1) + 'W';
        player2.matchesLost += 1;
        player2.lastFive = player2.lastFive.substring(1) + 'L';
    } else {
        player1.matchesLost += 1;
        player1.lastFive = player1.lastFive.substring(1) + 'L';
        player2.matchesWon += 1;
        player2.lastFive = player2.lastFive.substring(1) + 'W';
    }

    if (player1.gamesPlayed < 10) {
        if (player1Score > player2Score) {
            player1.skillRating += 25;
        } else {
            player1.skillRating -= 25;
        }
    } else if (player1.gamesPlayed >= 10) {
        player1.hasPlayedPlacement = "Y";
        player1.skillRating = skillAdjustments[0];
    }

    if (player2.gamesPlayed < 10) {
        if (player2Score > player1Score) {
            player2.skillRating += 25;
        } else {
            player2.skillRating -= 25;
        }
    } else if (player2.gamesPlayed >= 10) {
        player2.hasPlayedPlacement = "Y";
        player2.skillRating = skillAdjustments[1];
    }

    if (player2Score > player1Score) {
        console.log(`${player2.playerName} defeated ${player1.playerName}`);
    } else {
        console.log(`${player1.playerName} defeated ${player2.playerName}`);
    }    

    console.log(`${player1.playerName} moved from ${player1PreviousSkill} --> ${player1.skillRating}`);
    console.log(`${player2.playerName} moved from ${player2PreviousSkill} --> ${player2.skillRating}`);
    console.log('-----------------------------');

    player1.gamesPlayed += 1;
    player2.gamesPlayed += 1;
});

currentRankings.sort(function(player1, player2) {
    return player1.skillRating < player2.skillRating;
});

//update the list of round scores for each player
historicRankings.forEach(function(element) {
    var playedIndicator = 0;

    for (var i = 0; i < currentRankings.length; i++){
      if (currentRankings[i].playerName == element.playerName){
         element.rankings.push(currentRankings[i].skillRating);
         playedIndicator++;
      }
    }

    if (playedIndicator == 0) { //indicates they didn't play in this set of fixtures
        element.rankings.push(element.rankings[element.rankings.length-1]);
    }

});

//update rocketLeague level
currentRankings.forEach(function(element){
    var skillRating = element.skillRating;
    switch (true) {
        case (element.hasPlayedPlacement == 'N'): element.level = "unranked"; break;
        case (skillRating <= 500): element.level = "bronze1"; break;
        case (skillRating > 500 && skillRating <= 700): element.level = "bronze2"; break;
        case (skillRating > 700 && skillRating <= 900): element.level = "bronze3"; break;
        case (skillRating > 900 && skillRating <= 1100): element.level = "silver1"; break;
        case (skillRating > 1100 && skillRating <= 1300): element.level = "silver2"; break;
        case (skillRating > 1300 && skillRating <= 1500): element.level = "silver3"; break;
        case (skillRating > 1500 && skillRating <= 1700): element.level = "gold1"; break;
        case (skillRating > 1700 && skillRating <= 1900): element.level = "gold2"; break;
        case (skillRating > 1900 && skillRating <= 2100): element.level = "gold3"; break;
        case (skillRating > 2100 && skillRating <= 2300): element.level = "platinum1"; break;
        case (skillRating > 2300 && skillRating <= 2500): element.level = "platinum2"; break;
        case (skillRating > 2500 && skillRating <= 2700): element.level = "platinum3"; break;
        case (skillRating > 2700 && skillRating <= 2900): element.level = "diamond1"; break;
        case (skillRating > 2900 && skillRating <= 3100): element.level = "diamond2"; break;
        case (skillRating > 3100 && skillRating <= 3300): element.level = "diamond3"; break;
        case (skillRating > 3300 && skillRating <= 3500): element.level = "champion1"; break;
        case (skillRating > 3500 && skillRating <= 3700): element.level = "champion2"; break;
        case (skillRating > 3700 && skillRating <= 3900): element.level = "champion3"; break;
        case (skillRating > 3900 && skillRating <= 4100): element.level = "grandChampion"; break;
    }
});

console.log(currentRankings);
console.log(historicRankings);

fs.writeFileSync('./historic_rankings.json', JSON.stringify(historicRankings), 'utf-8');
return fs.writeFileSync('./rankings.json', JSON.stringify(currentRankings), 'utf-8');

